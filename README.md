#Netflix Movie Rating 

This repository is for trying out a solution for the infamous [Netflix Prize](https://en.wikipedia.org/wiki/Netflix_Prize) competition that happened way back in 2009. Though the problem is old, I found it pretty interesting and tried a solution. Hence, this repository. 

__Note :__ I hope that the problem am addressing is an open problem and the solution am providing is my own version. Feel free to contact me to discuss regarding optimization/improvement.

__Mail ID :__ g.vivek14@gmail.com

#Technical details

* Using [MovieLens stable rating dataset](http://grouplens.org/datasets/movielens/)
* Using ApacheSpark - Spark ML library