import os
from pyspark.sql.types import *
from pyspark.sql.functions import regexp_extract
from pyspark.sql import functions as F

# Just replace dbfs_dir with your local directory of movie dataset

if __name__ == "__main__":
    spark = SparkSession\
        .builder\
        .appName("NetflixMovieRating")\
        .getOrCreate()

        dbfs_dir = 'data-001'
        ratings_filename = dbfs_dir + '/ratings.csv'
        movies_filename = dbfs_dir + '/movies.csv'

    # Lets take a look at the dataset !

    display(dbutils.fs.ls(dbfs_dir))

    # Lets define our own schema 

    ratings_df_schema = StructType(
            [StructField('userId', IntegerType()),
            StructField('movieId', IntegerType()),
            StructField('rating', DoubleType())]
            )

    movies_df_schema = StructType(
            [StructField('ID', IntegerType()),
            StructField('title', StringType())]
            )

    # Lets load the data using the above schema 

    raw_ratings_df = sqlContext.read.format('com.databricks.spark.csv').options(header=True, inferSchema=False).schema(ratings_df_schema).load(ratings_filename)
    ratings_df = raw_ratings_df.drop('Timestamp')

    raw_movies_df = sqlContext.read.format('com.databricks.spark.csv').options(header=True, inferSchema=False).schema(movies_df_schema).load(movies_filename)
    movies_df = raw_movies_df.drop('Genres').withColumnRenamed('movieId', 'ID')

    # Caching it as we are going to access these data quite often

    ratings_df.cache()
    movies_df.cache()

    raw_ratings_count = raw_ratings_df.count()
    ratings_count = ratings_df.count()
    raw_movies_count = raw_movies_df.count()
    movies_count = movies_df.count()

    print 'There are %s ratings and %s movies in the datasets' % (ratings_count, movies_count)
    print 'Ratings:'
    ratings_df.show(3)
    print 'Movies:'
    movies_df.show(3, truncate=False)

    display(movies_df)
    display(ratings_df)

    movie_ids_with_avg_ratings_df = ratings_df.groupBy('movieId').agg(F.count(ratings_df.rating).alias("count"), F.avg(ratings_df.rating).alias("average"))
    print 'movie_ids_with_avg_ratings_df:'
    movie_ids_with_avg_ratings_df.show(3, truncate=False)

    movie_names_df = movie_ids_with_avg_ratings_df.join(movies_df, movie_ids_with_avg_ratings_df.movieId == movies_df.ID)
    movie_names_with_avg_ratings_df = movie_names_df.select('average','title','count','movieId')

    print 'movie_names_with_avg_ratings_df:'
    movie_names_with_avg_ratings_df.show(3, truncate=False)

    # Lets find Movies with Highest average Ratings and at least 1000 reviews

    movies_with_500_ratings_or_more = movie_names_with_avg_ratings_df.sort("average", ascending=False).filter("count >= 1000")
    print 'Movies with highest ratings:'
    movies_with_500_ratings_or_more.show(20, truncate=False)

    # Lets use Collaborative filtering for personalized predictions

    # Lets have 60% for training, 20% of our data for validation, and leave 20% for testing

    seed = 1800009193L
    (split_60_df, split_a_20_df, split_b_20_df) = ratings_df.randomSplit([0.6,0.2,0.2],seed)

    # Let's cache these datasets for performance
    training_df = split_60_df.cache()
    validation_df = split_a_20_df.cache()
    test_df = split_b_20_df.cache()

    print('Training: {0}, validation: {1}, test: {2}\n'.format(
                training_df.count(), validation_df.count(), test_df.count())
         )
    training_df.show(3)
    validation_df.show(3)
    test_df.show(3)

    # ALS

    # Lets initialize our ALS learner
    als = ALS()

    # Now we set the parameters for the method
    als.setMaxIter(5)\
        .setSeed(seed)\
        .setRegParam(0.1).setUserCol("userId").setItemCol("movieId").setRatingCol("rating")

    # Now let's compute an evaluation metric for our test dataset
        from pyspark.ml.evaluation import RegressionEvaluator

    # Create an RMSE evaluator using the label and predicted columns
        reg_eval = RegressionEvaluator(predictionCol="prediction", labelCol="rating", metricName="rmse")

        tolerance = 0.03
        ranks = [4, 8, 12]
        errors = [0, 0, 0]
        models = [0, 0, 0]
        err = 0
        min_error = float('inf')
        best_rank = -1

    # Create multiple models using ALS.fit(), one for each of our rank values. We'll fit against the training data set (training_df).

        for rank in ranks:
            als.setRank(rank)
            model = als.fit(training_df)

    # Run the model to create a prediction. Predict against the validation_df.
    predict_df = model.transform(validation_df)

    # Remove NaN values from prediction (due to SPARK-14489)
    predicted_ratings_df = predict_df.filter(predict_df.prediction != float('nan'))

    #For each model, we'll run a prediction against our validation data set (validation_df) and check the error.
    #We'll keep the model with the best error rate.
    # Run the previously created RMSE evaluator, reg_eval, on the predicted_ratings_df DataFrame
    error = reg_eval.evaluate(predicted_ratings_df)
        errors[err] = error
        models[err] = model
        print 'For rank %s the RMSE is %s' % (rank, error)
        if error < min_error:
        min_error = error
        best_rank = err
        err += 1

    als.setRank(ranks[best_rank])
        print 'The best model was trained with rank %s' % ranks[best_rank]
        my_model = models[best_rank]

    # In ML Pipelines, this next step has a bug that produces unwanted NaN values. We
    # have to filter them out. See https://issues.apache.org/jira/browse/SPARK-14489
    predict_df = my_model.transform(test_df)

    # Remove NaN values from prediction (due to SPARK-14489)
    predicted_test_df = predict_df.filter(predict_df.prediction != float('nan'))

    # Lets run the previously created RMSE evaluator, reg_eval, on the predicted_test_df DataFrame
    test_RMSE = reg_eval.evaluate(predicted_test_df)

    print('The model had a RMSE on the test set of {0}'.format(test_RMSE))

    # Compute the average rating
    avg_rating_df = training_df.select(F.avg(training_df.rating)).alias("average")#groupBy('movieId').agg(F.count(ratings_df.rating).alias("count"), F.avg(ratings_df.rating).alias("average"))
    print 'movie_ids_with_avg_ratings_df:'

    # Extract the average rating value. (This is row 0, column 0.)
    training_avg_rating = avg_rating_df.collect()[0][0]

    print('The average rating for movies in the training set is {0}'.format(training_avg_rating))

    # Add a column with the average rating
    test_for_avg_df = test_df.withColumn('prediction',F.lit(training_avg_rating) )

    # Run the previously created RMSE evaluator, reg_eval, on the test_for_avg_df DataFrame
    test_avg_RMSE = reg_eval.evaluate(test_for_avg_df)

    print("The RMSE on the average set is {0}".format(test_avg_RMSE))

    # We have predicted how users will rate movies !
    # Now lets do predictions for ourself 

    print 'Most rated movies:'
    print '(average rating, movie name, number of reviews, movie ID)'
    display(movies_with_500_ratings_or_more.orderBy(movies_with_500_ratings_or_more['average'].desc()).take(50))

    from pyspark.sql import Row
    my_user_id = 0

    my_rated_movies = [
        (my_user_id, 318, 5),
        (my_user_id, 858, 4),
        (my_user_id, 50, 3),
        (my_user_id, 527, 4),
        (my_user_id, 1221, 3),
        (my_user_id, 2019, 2),
        (my_user_id, 904, 5),
        (my_user_id, 7502, 4),
        (my_user_id, 912, 4),
        (my_user_id, 922, 5)
    ]

    my_ratings_df = sqlContext.createDataFrame(my_rated_movies, ['userId','movieId','rating'])
    print 'My movie ratings:'
    display(my_ratings_df.limit(10))

    training_with_my_ratings_df = training_df.unionAll(my_ratings_df)

    print ('The training dataset now has %s more entries than the original training dataset' %
            (training_with_my_ratings_df.count() - training_df.count()))

    # Reset the parameters for the ALS object.
    als.setPredictionCol("prediction")\
        .setMaxIter(5)\
        .setSeed(seed)\
        .setRegParam(0.1).setUserCol("userId").setItemCol("movieId").setRatingCol("rating")

    # Create the model with these parameters.
    my_ratings_model = als.fit(training_with_my_ratings_df)

    my_predict_df = my_ratings_model.transform(test_df)

    # Remove NaN values from prediction (due to SPARK-14489)
    predicted_test_my_ratings_df = my_predict_df.filter(my_predict_df.prediction != float('nan'))

    # Run the previously created RMSE evaluator, reg_eval, on the predicted_test_my_ratings_df DataFrame
    test_RMSE_my_ratings = reg_eval.evaluate(predicted_test_my_ratings_df)
    print('The model had a RMSE on the test set of {0}'.format(test_RMSE_my_ratings))

    # Create a list of my rated movie IDs
    my_rated_movie_ids = [x[1] for x in my_rated_movies]

    # Filter out the movies I already rated.
    not_rated_df = movies_df.filter(~ movies_df["ID"].isin(my_rated_movie_ids))

    # Rename the "ID" column to be "movieId", and add a column with my_user_id as "userId".
    my_unrated_movies_df = not_rated_df.withColumnRenamed("ID","movieId").withColumn('userId',F.lit(my_user_id) )

    # Use my_rating_model to predict ratings for the movies that I did not manually rate.
    raw_predicted_ratings_df = my_ratings_model.transform(my_unrated_movies_df)

    predicted_ratings_df = raw_predicted_ratings_df.filter(raw_predicted_ratings_df['prediction'] != float('nan'))

    predicted_with_counts_df = predicted_ratings_df.join(movie_names_with_avg_ratings_df, predicted_ratings_df.movieId == movie_names_with_avg_ratings_df.movieId)
    predicted_highest_rated_movies_df = predicted_with_counts_df.sort("average", ascending=False).filter("count>75")

    print ('My 25 highest rated movies as predicted (for movies with more than 75 reviews):')
    predicted_highest_rated_movies_df.show(25)

    spark.stop()
